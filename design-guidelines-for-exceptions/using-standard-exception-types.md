# Using Standard Exception Types

* * * * * 

This section describes the standard exceptions provided by the Framework
and the details of their usage. The list is by no means exhaustive.
Please refer to the .NET Framework reference documentation for usage of
other Framework exception types.

## Exception and SystemException

*X DO NOT throw
[`System.Exception`](https://msdn.microsoft.com/en-us/library/system.exception(v=vs.110).aspx)
or
[`System.SystemException`](https://msdn.microsoft.com/en-us/library/system.systemexception(v=vs.110).aspx).*

*X DO NOT catch `System.Exception` or `System.SystemException` in framework
code, unless you intend to rethrow.*

*X AVOID catching `System.Exception` or `System.SystemException`, except in
top-level exception handlers.*

## ApplicationException

*X DO NOT throw or derive from
[`ApplicationException`](https://msdn.microsoft.com/en-us/library/system.applicationexception(v=vs.110).aspx).*

## InvalidOperationException

**✓ DO throw an
[`InvalidOperationException`](https://msdn.microsoft.com/en-us/library/system.invalidoperationexception(v=vs.110).aspx)
if the object is in an inappropriate state.**

## ArgumentException, ArgumentNullException, and ArgumentOutOfRangeException

**✓ DO throw
[`ArgumentException`](https://msdn.microsoft.com/en-us/library/system.argumentexception(v=vs.110).aspx)
or one of its subtypes if bad arguments are passed to a member. Prefer
the most derived exception type, if applicable.**

**✓ DO set the ParamName property when throwing one of the subclasses of
`ArgumentException`.**

This property represents the name of the parameter that caused the
exception to be thrown. Note that the property can be set using one of
the constructor overloads.

**✓ DO use value for the name of the implicit value parameter of property
setters.**

## NullReferenceException, IndexOutOfRangeException, and AccessViolationException

*X DO NOT allow publicly callable APIs to explicitly or implicitly throw
[`NullReferenceException`](https://msdn.microsoft.com/en-us/library/system.nullreferenceexception(v=vs.110).aspx),
[`AccessViolationException`](https://msdn.microsoft.com/en-us/library/system.accessviolationexception(v=vs.110).aspx),
or
[`IndexOutOfRangeException`](https://msdn.microsoft.com/en-us/library/system.indexoutofrangeexception(v=vs.110).aspx).
These exceptions are reserved and thrown by the execution engine and in
most cases indicate a bug.*

Do argument checking to avoid throwing these exceptions. Throwing these
exceptions exposes implementation details of your method that might
change over time.

## StackOverflowException

*X DO NOT explicitly throw
[`StackOverflowException`](https://msdn.microsoft.com/en-us/library/system.stackoverflowexception(v=vs.110).aspx).
The exception should be explicitly thrown only by the CLR.*

*X DO NOT catch StackOverflowException.*

It is almost impossible to write managed code that remains consistent in
the presence of arbitrary stack overflows. The unmanaged parts of the
CLR remain consistent by using probes to move stack overflows to
well-defined places rather than by backing out from arbitrary stack
overflows.

## OutOfMemoryException

*X DO NOT explicitly throw
[`OutOfMemoryException`](https://msdn.microsoft.com/en-us/library/system.outofmemoryexception(v=vs.110).aspx).
This exception is to be thrown only by the CLR infrastructure.*

## ComException, SEHException, and ExecutionEngineException

*X DO NOT explicitly throw [`COMException`](https://msdn.microsoft.com/en-us/library/system.runtime.interopservices.comexception(v=vs.110).aspx),
[`ExecutionEngineException`](https://msdn.microsoft.com/en-us/library/system.executionengineexception(v=vs.110).aspx),
and
[`SEHException`](https://msdn.microsoft.com/en-us/library/system.runtime.interopservices.sehexception(v=vs.110).aspx).
These exceptions are to be thrown only by the CLR infrastructure.*
