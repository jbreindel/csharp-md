# Equality Operators

* * * * *

This section discusses overloading equality operators and refers to
operator== and operator!= as equality operators.

*X DO NOT overload one of the equality operators and not the other.*

**✓ DO ensure that
[`Object.Equals`](https://msdn.microsoft.com/en-us/library/w4hkze5k(v=vs.110).aspx)
and the equality operators have exactly the same semantics and similar
performance characteristics.**

This often means that Object.Equals needs to be overridden when the
equality operators are overloaded.

*X AVOID throwing exceptions from equality operators.*

For example, return false if one of the arguments is null instead of
throwing `NullReferenceException`.

## Equality Operators on Value Types

* * * * *

**✓ DO overload the equality operators on value types, if equality is
meaningful.**

In most programming languages, there is no default implementation of
`operator==` for value types.

## Equality Operators on Reference Types

* * * * *

*X AVOID overloading equality operators on mutable reference types.*

Many languages have built-in equality operators for reference types. `The
built-in operators usually implement the reference equality, and many
developers are surprised when the default behavior is changed to the
value equality`.

This problem is mitigated for immutable reference types because
immutability makes it much harder to notice the difference between
reference equality and value equality.

*X AVOID overloading equality operators on reference types if the
implementation would be significantly slower than that of reference
equality.*

