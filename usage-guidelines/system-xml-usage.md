## System.Xml Usage

* * * * *

This section talks about usage of several types residing in
[`System.Xml`](https://msdn.microsoft.com/en-us/library/system.xml(v=vs.110).aspx)
namespaces that can be used to represent XML data.

*X DO NOT use
[`XmlNode`](https://msdn.microsoft.com/en-us/library/system.xml.xmlnode(v=vs.110).aspx)
or
[`XmlDocument`](https://msdn.microsoft.com/en-us/library/system.xml.xmldocument(v=vs.110).aspx)
to represent XML data. Favor using instances of
[`IXPathNavigable`](https://msdn.microsoft.com/en-us/library/system.xml.xpath.ixpathnavigable(v=vs.110).aspx),
[`XmlReader`](https://msdn.microsoft.com/en-us/library/system.xml.xmlreader(v=vs.110).aspx),
[`XmlWriter`](https://msdn.microsoft.com/en-us/library/system.xml.xmlwriter(v=vs.110).aspx),
or subtypes of
[`XNode`](https://msdn.microsoft.com/en-us/library/system.xml.linq.xnode(v=vs.110).aspx)
instead. `XmlNode` and `XmlDocument` are not designed for exposing in public
APIs.*

**✓ DO use `XmlReader`, `IXPathNavigable`, or subtypes of `XNode` as input or
output of members that accept or return XML.**

Use these abstractions instead of `XmlDocument`, `XmlNode`, or
[`XPathDocument`](https://msdn.microsoft.com/en-us/library/system.xml.xpath.xpathdocument(v=vs.110).aspx),
because this decouples the methods from specific implementations of an
in-memory XML document and allows them to work with virtual XML data
sources that expose `XNode`, `XmlReader`, or
[`XPathNavigator`](https://msdn.microsoft.com/en-us/library/system.xml.xpath.xpathnavigator(v=vs.110).aspx).

*X DO NOT subclass `XmlDocument` if you want to create a type representing
an XML view of an underlying object model or data source.*

