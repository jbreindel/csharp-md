# Unsealed Classes

* * * * *

`Sealed classes cannot be inherited from`, and they prevent extensibility.
In contrast, classes that can be inherited from are called unsealed
classes.

**✓ CONSIDER using unsealed classes with no added virtual or protected
members as a great way to provide inexpensive yet much appreciated
extensibility to a framework.**

Developers often want to inherit from unsealed classes so as to add
convenience members such as custom constructors, new methods, or method
overloads. For example, `System.Messaging.MessageQueue` is unsealed and
thus allows users to create custom queues that default to a particular
queue path or to add custom methods that simplify the API for specific
scenarios.

Classes are unsealed by default in most programming languages, and this
is also the recommended default for most classes in frameworks. The
extensibility afforded by unsealed types is much appreciated by
framework users and quite inexpensive to provide because of relatively
low test costs associated with unsealed types.
