
# Static Class Design

* * * *

A static class is defined as a class that contains only static members
(of course besides the instance members inherited from
[System.Object](https://msdn.microsoft.com/en-us/library/system.object(v=vs.110).aspx)
and possibly a private constructor). Some languages provide built-in
support for static classes. In C# 2.0 and later, when a class is
declared to be static, it is sealed, abstract, and no instance members
can be overridden or declared.

Static classes are a compromise between pure object-oriented design and
simplicity. They are commonly used to provide shortcuts to other
operations (such as
[System.IO.File](https://msdn.microsoft.com/en-us/library/system.io.file(v=vs.110).aspx)),
holders of extension methods, or functionality for which a full
object-oriented wrapper is unwarranted (such as
[System.Environment](https://msdn.microsoft.com/en-us/library/system.environment(v=vs.110).aspx)).

**✓ DO use static classes sparingly.**

`Static classes should be used only as supporting classes` for the
object-oriented core of the framework.

*X DO NOT treat static classes as a miscellaneous bucket.*

*X DO NOT declare or override instance members in static classes.*

**✓ DO declare static classes as sealed, abstract, and add a private
instance constructor if your programming language does not have built-in
support for static classes.**
